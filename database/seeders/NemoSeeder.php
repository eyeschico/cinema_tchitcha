<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class NemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listNemo = Http::get('http://www.omdbapi.com/?s=nemo&apikey=8600fae4&page=1');
        foreach($listNemo['Search'] as $film){
            $detail1film= Http::get('http://www.omdbapi.com/?i='.$film['imdbID'].'&apikey=8600fae4');
            
            DB::table('movies')->insert([
            'movie_name' => $detail1film['Title'],
            'duree' => $detail1film['Runtime'],
            'annee_sortie' => $detail1film['Year'],
            'image' => $detail1film['Poster'],
            'synopsis' => $detail1film['Plot'],
            'director' => $detail1film['Director'],        
        ]);

    }
}
}

