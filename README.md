### Projet mener par Myriam, Mohammed et Amelle

## Bienvenue sur TchiTcha

#### Pour l'installation en local:

- Installer les composants avec la commande : "composer install" et "npm install",
- Crée un fichier .env en prenant pour exemple le fichier .env.example,
- Modifier  DB_DATABASE, DB_USERNAME, DB_PASSWORD,
- "php artisan serve" pour ouvir avec votre serveur local.
            
# Notre organisation sur Asana

https://app.asana.com/0/1199908569580156/board 

# Notre maquette sur Figma

https://www.figma.com/file/OznzwZvFE5YKbPMuazWPNq/Maquette-Cin%C3%A9ma-TchiTcha-w%2F-Amelle%2C-Myriam-and-Mohammed?node-id=15%3A92

# Notre site web en ligne

https://tchitcha.herokuapp.com