@extends("layouts.app")
@section("content")
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-6">
			<h1>{{ $movie->movie_name }}</h1>
			<h3>{{$movie->director}}</h3>
			<div class="mb-3" >{!! nl2br($movie->synopsis) !!}</div>
			<div class="bg-white mt-3 p-3 border text-center" >	
				<p>Valider <strong>{{ $movie->movie_name }}</strong></p>
				<form method="POST" action="#" class="form-inline d-inline-block" >
					@csrf
					<input type="number" name="quantity" placeholder="Quantité ?" class="form-control mr-2" >
					<button type="submit" class="btn btn-warning" >+ ajout</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection