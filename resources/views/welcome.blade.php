@extends('layouts.head')

@section('contenu')
<body style="
            background-color:black;
            background-image: url('https://images.unsplash.com/photo-1489599849927-2ee91cede3ba?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1650&q=80');
            background-position: center;
            background-repeat: no-repeat;
            background-attachment: fixed;">

<div class="container-fluid">
    @extends('layouts.style')
    <h1 class="text-white p-4" style="font-family: 'Abril Fatface', cursive !important; font-size: 50px"> <a class="text-decoration-none" style="color: white;" href="{{ url('/') }}">TchiTcha</a></h1>

    <div class="container mb-5" style="
    background: rgba( 2, 2, 2, 0.40 );
    box-shadow: 0 8px 32px 0 rgba( 31, 38, 135, 0.37 );
    backdrop-filter: blur( 6.0px );
    -webkit-backdrop-filter: blur( 6.0px );
    border-radius: 10px;
    border: 3px solid blue;">
        <h1 class="text-white p-5">Louez vos films et séries préférés comme a l'ancienne</h1>

        <div class="row m-4">
            <div class="col-sm mb-5">
                <img class="rounded-3" src="https://images.unsplash.com/photo-1593784991188-c899ca07263b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1650&q=80" alt="image d'une télévision" width="100%">
            </div>
            <div class="col-sm">
                <h2 class="text-white mb-5">Envie de tenter l'éxperience ?</h2>
                <h4 class="text-white p-3 rounded-3" style="background-color: white !important;" ><a class="text-decoration-none" href="{{ route('register') }}">Rejoignez la communauté TchiTcha !</a></h4>
                <h4 class="text-white p-3 rounded-3" style="background-color: white !important;"><a class="text-decoration-none" href="{{ route('login') }}">Vous êtes déja membre ? Connectez-vous</a></h4>
            </div>
        </div>
    </div>
</div>

</body>
@endsection