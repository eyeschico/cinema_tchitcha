@extends('layouts.head')

@section('contenu')
<body style="
            background-color:black;
            background-image: url('https://images.unsplash.com/photo-1489599849927-2ee91cede3ba?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1650&q=80');
            background-position: center;
            background-repeat: no-repeat;
            background-attachment: fixed;">

    @extends('layouts.style')
    <h1 class="text-white p-4" style="font-family: 'Abril Fatface', cursive !important;"> <a class="text-decoration-none" style="color: white;" href="{{ url('/') }}">TchiTcha</a></h1>

    <div class="container-sm p-5 rounded shadow" style="
                background: rgba( 0, 0, 0, 0.20 );
                box-shadow: 0 8px 32px 0 rgba( 31, 38, 135, 0.37 );
                backdrop-filter: blur( 6.0px );
                -webkit-backdrop-filter: blur( 6.0px );
                border: 2px solid magenta;">

        <h1 class="text-white">{{ __('Inscription') }}</h1>

                <form method="POST" action="{{ route('register') }}" class="text-white" >
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">{{ __('Nom') }}</label>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                
                <div class="mb-3">
                    <label for="email" class="form-label">{{ __('E-Mail') }}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">              
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="password" class="form-label">{{ __('Mot de passe') }}</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="mb-3">
                    <label for="password-confirm" class="form-label">{{ __('Confirmation du mot de passe') }}</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="mb-3">
                <button type="submit" class="btn btn-primary" style="background-color: magenta !important; border: none !important">{{ __('Inscription') }}</button>
                </div>

                </form>       
    </div>
</body>
@endsection
