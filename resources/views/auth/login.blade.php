@extends('layouts.head')

@section('contenu')
<body style="
            background-color:black;
            background-image: url('https://images.unsplash.com/photo-1489599849927-2ee91cede3ba?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1650&q=80');
            background-position: center;
            background-repeat: no-repeat;
            background-attachment: fixed;">

    @extends('layouts.style')
    <h1 class="text-white p-4" style="font-family: 'Abril Fatface', cursive !important;"> <a class="text-decoration-none" style="color: white;" href="{{ url('/') }}">TchiTcha</a></h1>


    <div class="container-sm p-5 rounded shadow" style="
                margin-top: 5vh !important;
                background: rgba( 0, 0, 0, 0.20 );
                box-shadow: 0 8px 32px 0 rgba( 31, 38, 135, 0.37 );
                backdrop-filter: blur( 6.0px );
                -webkit-backdrop-filter: blur( 6.0px );
                border: 2px solid red;">

        <h1 class="text-white">S'identifier</h1>

                <form method="POST" action="{{ route('login') }}" class="text-white" >
                @csrf
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">{{ __('E-Mail') }}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">{{ __('Mot de passe') }}</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">              
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="mb-3 form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label" for="remember" style="color: grey !important;">
                                {{ __('Se souvenir de moi') }}
                            </label>
                </div>
                <button type="submit" class="btn btn-primary" style="background-color: red !important; border: none !important">
                        {{ __('Connexion') }}
                    </button>
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}" style="color: white !important;">
                                {{ __('Mot de passe oublié ?') }}
                            </a>
                        @endif
                <p class="mt-3" style="color: grey !important;">Première visite sur TchiTcha ? <a href="{{ route('register') }}" style="color: white !important;">Inscrivez-vous !</a></p>
                </form>       
    </div>
</body>
@endsection
