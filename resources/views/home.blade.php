@extends('layouts.app')

@section('content')

<body style="background-color:#0f0814;">
    
    <h1 class="mx-auto rounded shadow" style="width: 19rem; color: #FFFFFF">Bonjour {{ Auth::user()->name}} !</h1>

    <div class="container">
    <div class="row">
        <div class="col-sm">
            <p style="color: #FFFFFF">Sort by :</p>

                <a href="{{route('sortByTitle')}}"><button type="button" class="btn btn-outline-secondary">Title</button></a>
                <a href="{{route('sortByYear')}}"><button type="button" class="btn btn-outline-secondary">Year</button></a>
                <a href="{{route('sortByRuntime')}}"><button type="button" class="btn btn-outline-secondary">Runtime</button></a>
        </div>

        <div class="col-sm">
            <p style="color: #FFFFFF">Filter by :</p>

                <a href="{{route('filterByAvailable')}}"><button type="button" class="btn btn-outline-secondary">Available</button></a>
                <a href="{{route('filterByUnavailable')}}"><button type="button" class="btn btn-outline-secondary">Unavailable</button></a>
        </div>        
        <div class="col-sm">
        </div>
    </div>
    </div>

    <div class="container" style="height: 100vh !important">
    <div class="row">
        @foreach ($movies as $movie)
            <div class="col bg-grey m-3" style="
            
            background: rgba( 0, 0, 0, 0.40 );
            box-shadow: 0 8px 32px 0 rgba( 31, 38, 135, 0.37 );
            backdrop-filter: blur( 6.5px );
            -webkit-backdrop-filter: blur( 6.5px );
            border-radius: 10px;
            border: 2px solid blue;">
                <img src="{{$movie->image}}" class="card-img-top p-5" alt="...">
                <div class="card-body">
                    <h4 class="card-title text-white">{{$movie->movie_name}}</h4>
                    <h6 class="card-title text-white">{{$movie->director}}</h6>
                    <h6 class="card-title text-white">{{$movie->duree}}</h6>
                    <h6 class="card-title text-white">{{$movie->annee_sortie}}</h6>
                    <p class="card-text text-white">{{$movie->synopsis}}</p>
                </div>
                <div class="card-footer col">
                    <h5><span class="badge bg-success">Disponible</span></h5>
                    <h5><span class="badge bg-danger">Indisponible</span></h5>
                    <button type="button" class="btn btn-warning">Rendre ce film</button>
                    <form method="POST" action="{{ route('caddy.add', $movie) }}">
                        @csrf
                        <button type="submit" class="btn btn-info mt-2 mb-5" style="width: 250px;">Ajouter au panier</button>
                    </form>
                </div>
                
            </div>
        @endforeach
        </div>
    </div>
</body>

@endsection('content')