@extends("layouts.app")
@section("content")
<body style="background-color:#0f0814;">
	

<div class="container">

	@if (session()->has('message'))
	<div class="alert alert-info">{{ session('message') }}</div>
	@endif

	@if (session()->has("caddy"))
	<h1 class="text-white">Mon panier</h1>
	<div class="table-responsive shadow mb-3">
		<table class="table table-bordered table-hover bg-white mb-0">
			<thead class="thead-dark" >
				<tr>
					<th>Film</th>
					<th>Director</th>
					<th>Année de sortie</th>
				</tr>
			</thead>
			<tbody>
			@foreach (session("caddy") as $key => $item)
					<tr>
						<td>
							<strong><p>{{ $item['movie_name'] }}</p></strong>
						</td>
						<td>
							<p>{{ $item['director'] }}</p>
						</td>
						<td>
							<p>{{ $item['annee_sortie'] }}</p>
						</td>
					</tr>
					
				@endforeach
			</tbody>

		</table>
	</div>

	<!-- Lien pour vider le panier -->
	<a class="btn btn-danger" href="{{ route('caddy.empty') }}" title="Retirer tous les produits du panier" >Vider le panier</a>

	@else
	<div class="alert alert-info">Aucun produit au panier</div>
	@endif

</div>
</body>
@endsection