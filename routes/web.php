<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\CaddieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Page de connexion/inscription
Route::get('/', function () {
    return view('welcome');
});

//Page d'accueil/liste de films

    //Filtres des films
    Route::get('/sort=title', [MovieController::class, 'sortByTitle'])->name('sortByTitle');
    Route::get('/sort=annee_sortie', [MovieController::class, 'sortByYear'])->name('sortByYear');
    Route::get('/sort=duree', [MovieController::class, 'sortByRuntime'])->name('sortByRuntime');

    Route::get('/filter=status', [MovieController::class, 'filterByAvailable'])->name('filterByAvailable');
    Route::get('/filter=duree', [MovieController::class, 'filterByUnavailable'])->name('filterByUnavailable');


// Route::get('/home', function () {
//     return view('home');
// });

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [MovieController::class, 'afficheLaListe'])->name('home');

//Panier
Route::get('/caddy', function () {
    return view('caddy');
});


Route::get('/caddy', "App\Http\Controllers\CaddieController@show")->name('caddy.show');
Route::post('/caddy/add/{movie}', "App\Http\Controllers\CaddieController@add")->name('caddy.add');
Route::get('/caddy/remove/{movie}', "App\Http\Controllers\CaddieController@remove")->name('caddy.remove');
Route::get('/caddy/empty', "App\Http\Controllers\CaddieController@empty")->name('caddy.empty');
Route::get('/caddy/validate', "App\Http\Controllers\CaddieController@validate")->name('caddy.validate');

//Movie
Route::get('/movie', "App\Http\Controllers\MovieController@index")->name('movie.index');
Route::get('/movie', "App\Http\Controllers\MovieController@store")->name('movie.store');
Route::get('movie', "App\Http\Controllers\MovieController@show")->name('movie.show');
Route::get('movie/create/{movie}', "App\Http\Controllers\MovieController@create")->name('movie.create');
Route::delete('movie/destroy/{movie}', "App\Http\Controllers\MovieController@destroy")->name('movie.destroy');
Route::delete('movie/{movie}/edit', "App\Http\Controllers\MovieController@edit")->name('movie.edit');


Auth::routes();


Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

