<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\BasketInterfaceRepository;
use App\Models\Movie;

class CaddieController extends Controller
{

	protected $basketRepository; // L'instance BasketSessionRepository

    public function __construct (BasketInterfaceRepository $basketRepository) {
    	$this->basketRepository = $basketRepository;
    }

    # Affichage du panier
    public function show () {
    	return view("caddy.show"); // resources\views\basket\show.blade.php
    }

    # Ajout d'un produit au panier
    public function add (Movie $movie, Request $request) {
    	
    	// Validation de la requête
    	$this->validate($request, [
    		"quantity" => "numeric|min:1"
    	]);

    	// Ajout/Mise à jour du produit au panier avec sa quantité
    	$this->basketRepository->add($movie, $request->quantity);

    	// Redirection vers le panier avec un message
    	return redirect()->route("caddy.show")->withMessage("Produit ajouté au panier");
    }

    // Suppression d'un produit du panier
    public function remove (Movie $movie) {

    	// Suppression du produit du panier par son identifiant
    	$this->basketRepository->remove($movie);

    	// Redirection vers le panier
    	return back()->withMessage("Produit retiré du panier");
    }

    // Vider la panier
    public function empty () {

    	// Suppression des informations du panier en session
    	$this->basketRepository->empty();

    	// Redirection vers le panier
    	return back()->withMessage("Panier vidé");

    }

}
