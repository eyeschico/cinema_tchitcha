<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Movie;

class MovieController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    function afficheLaListe(){
        return view('/home', ['movies' => Movie::all()]);
    }

    public function show(Movie $movie){
      return view("movies.movie", compact("movie"));
    }

    function sortByTitle(){
        return view('/home', ['movies' => Movie::orderBy('movie_name')->get()]);
    }

    function sortByYear(){
        return view('/home', ['movies' => Movie::orderBy('annee_sortie')->get()]);
    }

    function sortByRuntime(){
        return view('/home', ['movies' => Movie::orderBy('duree')->get()]);
    }

    function filterByAvailable(){
        return view('/home', ['movies' => Movie::where('status','available')->get()]);
    }

    function filterByUnavailable(){
        return view('/home', ['movies' => Movie::where('status','unavailable')->get()]);
    }
}