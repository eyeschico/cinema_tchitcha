<?php

namespace App\Http;

use App\Models\Movie;

interface BasketInterfaceRepository {

	// Afficher le panier
	public function show();

	// Ajouter un produit au panier
	public function add(Movie $movie);

	// Retirer un produit du panier
	public function remove(Movie $movie);

	// Vider le panier
	public function empty();

}

?>