<?php

namespace App\Http;

use App\Models\Movie;



class BasketSessionRepository implements BasketInterfaceRepository  {

	# Afficher le panier
	public function show () {
		return view("caddy.show"); // resources\views\basket\show.blade.php
	}

	# Ajouter/Mettre à jour un produit du panier
	public function add (Movie $movie) {		
		$caddy = session()->get("caddy"); // On récupère le panier en session

		// Les informations du produit à ajouter
		$movie_details = [
			'id' => $movie->id,
			'movie_name' => $movie->movie_name,
			'director' => $movie->director,
            'synopsis' => $movie->synopsis,
            'image' => $movie->image,
            'annee_sortie' => $movie->annee_sortie,
		];
		
		$caddy[$movie->id] = $movie_details; // On ajoute ou on met à jour le produit au panier
		session()->put("caddy", $caddy); // On enregistre le panier
	}

	# Retirer un produit du panier
	public function remove (Movie $movie) {
		$caddy = session()->get("caddy"); // On récupère le panier en session
		unset($caddy[$movie->id]); // On supprime le produit du tableau $basket
		session()->put("caddy", $movie); // On enregistre le panier
	}


	# Vider le panier
	public function empty () {
		session()->forget("caddy"); // On supprime le panier en session
	}

}

?>